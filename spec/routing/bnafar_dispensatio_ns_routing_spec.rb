require "rails_helper"

RSpec.describe BnafarDispensatioNsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/bnafar_dispensations").to route_to("bnafar_dispensations#index")
    end

    it "routes to #show" do
      expect(:get => "/bnafar_dispensations/1").to route_to("bnafar_dispensations#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/bnafar_dispensations").to route_to("bnafar_dispensations#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/bnafar_dispensations/1").to route_to("bnafar_dispensations#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/bnafar_dispensations/1").to route_to("bnafar_dispensations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/bnafar_dispensations/1").to route_to("bnafar_dispensations#destroy", :id => "1")
    end
  end
end
