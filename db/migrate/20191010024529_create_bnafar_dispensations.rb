class CreateBnafarDispensations < ActiveRecord::Migration[5.2]
  def change
    create_table :bnafar_dispensations do |t|
      t.date :NU_COMPETENCIA_DISPENSACAO
      t.string :NU_CATMAT
      t.integer :CO_PRINCIPIO_ATIVO_MEDICAMENTO
      t.decimal :VL_UNITARIO, precision: 20, scale: 2
      t.decimal :VL_UNITARIO_MIN, precision: 20, scale: 2
      t.decimal :VL_UNITARIO_MAX, precision: 20, scale: 2
      t.decimal :VL_UNITARIO_MEDIAN, precision: 20, scale: 2
      t.decimal :MONTANTE_DISPENSADA, precision: 20, scale: 2
      t.integer :CO_GRUPO_FINANCIAMENTO
      t.string :SG_PROGRAMA_SAUDE
      t.integer :CO_MUNICIPIO_IBGE_EST
      t.string :COORD
      t.string :ORIGIN

      t.timestamps
    end
  end
end
