class CreateBudgets < ActiveRecord::Migration[5.2]
  def change
    create_table :budgets do |t|
      t.integer :ano_exercicio
      t.string :acao
      t.string :localizador
      t.string :plano_orcamentario
      t.string :natureza_despesa
      t.decimal :vl_ploa
      t.decimal :vl_dotacao_atual
      t.decimal :vl_empenhado
      t.decimal :vl_empenhado_a_liquidar
      t.decimal :vl_pago
      t.decimal :vl_rap_np

      t.timestamps
    end
  end
end
