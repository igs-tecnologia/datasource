class CreateBnafarRemedies < ActiveRecord::Migration[5.2]
  def change
    create_table :bnafar_remedies do |t|
      t.string :DS_PRODUTO
      t.string :NU_CATMAT
      t.string :CO_GRUPO_FINANCIAMENTO

      t.timestamps
    end
  end
end
