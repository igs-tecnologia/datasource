SELECT 
    YEAR(NU_COMPETENCIA_DISPENSACAO) AS ANO,
    MONTH(NU_COMPETENCIA_DISPENSACAO) AS MES,
    CO_MUNICIPIO_IBGE_EST,
    SUM(MONTANTE_DISPENSADA) AS MONTANTE_DISPENSADA,
    NU_CATMAT,#TODO numero anvisa? qtd unitarias?
    SUM(VL_UNITARIO * MONTANTE_DISPENSADA) AS VL_PAGO
FROM datasource_development.bnafar_dispensations
WHERE CO_MUNICIPIO_IBGE_EST != -1 AND CO_MUNICIPIO_IBGE_EST IS NOT null AND YEAR(NU_COMPETENCIA_DISPENSACAO) <= 2019
GROUP BY CO_MUNICIPIO_IBGE_EST, NU_CATMAT, ANO, MES
ORDER BY ANO
LIMIT 2;