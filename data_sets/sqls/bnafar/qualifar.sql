-- Total IBGE: 5.570
-- Total Base: 5.612

# QtdMunEst & QtdMunPac
/*
SELECT 
	COUNT(DISTINCT(CO_MUNICIPIO_IBGE_EST)) AS QtdMunEst, 
	COUNT(DISTINCT(CO_MUNICIPIO_IBGE_PAC)) AS QtdMunPac
FROM datasource_development.bnafar_dispensations;
*/

# Unique Mun considering digits (6 or 7 str length)
SELECT 
	CO_MUNICIPIO_IBGE_EST
FROM datasource_development.bnafar_dispensations
	WHERE CO_MUNICIPIO_IBGE_EST >= 1000000;
