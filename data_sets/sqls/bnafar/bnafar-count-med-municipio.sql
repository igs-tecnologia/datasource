-- Jacareí no BD: 352440; Jacareí no IBGE: 3524402
SELECT count(distinct(CO_MUNICIPIO_IBGE_EST)) AS QTD_MUNICIPIOS
FROM datasource_development.bnafar_dispensations
WHERE CO_MUNICIPIO_IBGE_EST != -1 AND CO_MUNICIPIO_IBGE_EST IS NOT null;