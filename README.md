# DataSource
DataSource CRUDs for data tests

# "SIAFI/SIOP" Model/API
Code to generate BUDGET CRUD Model:
```shell script
  $ rails g scaffold Budget ano_exercicio:integer acao:string localizador:string plano_orcamentario:string natureza_despesa:string vl_ploa:decimal vl_dotacao_atual:decimal vl_empenhado:decimal vl_empenhado_a_liquidar:decimal vl_pago:decimal vl_rap_np:decimal
```

Code to remove generated code:
```shell script
  $ rails d scaffold Budget
```

Code to generate BNAFAR_DISPENSATION CRUD Model:
```shell script
  $ rails g scaffold BNAFAR_DISPENSATION NU_COMPETENCIA_DISPENSACAO:date NU_CATMAT:string CO_PRINCIPIO_ATIVO_MEDICAMENTO:integer VL_UNITARIO:decimal VL_UNITARIO_MIN:decimal VL_UNITARIO_MAX:decimal VL_UNITARIO_MEDIAN:decimal MONTANTE_DISPENSADA:decimal CO_GRUPO_FINANCIAMENTO:integer SG_PROGRAMA_SAUDE:string CO_MUNICIPIO_IBGE_EST:integer COORD:string ORIGIN:string
``` 
Decimal (VL e Montante): , precision: 18, scale: 2
Rename Bnafar to BNAFAR

Code to generate REMEDY CRUD Model:
```shell script
  $ rails g scaffold BNAFARRemedy DS_PRODUTO:string NU_CATMAT:string CO_GRUPO_FINANCIAMENTO:string
```

Code to generate CATMAT CRUD Model:
```shell script
  $ rails g scaffold BNAFARCATMAT NU_PRODUTO:string NU_PROCEDIMENTO:string CO_SEQ_PRODUTO:integer CO_GRUPO_RENAME:integer CO_CLASSE_RENAME:string CO_SUBCLASSE_RENAME:string CO_PRINCIPIO_ATIVO_MEDICAM:string CO_CONCENTRACAO_MEDIC:string NU_CONCENTRACAO_MEDIC:string CO_FORMULA_FARMACEUTICA:string CO_VOLUME_MEDICAMENTO:string NU_VOLUME_MEDICAMENTO:string CO_LISTA:string CO_TIPO_PRODUTO:string CO_UNIDADE_MEDIDA:string CO_MUNICIPIO_IBGE_HORUS:string CO_ESFERA_ADMINISTRATIVA_HORUS:string DS_PRODUTO:string ST_REGISTRO_ATIVO:string CO_SUBGRUPO_QUIMICO:string NU_FATOR_CORRECAO:string CO_UNIDADE_CONSUMO:string ST_DISPENSACAO_COMPOSICAO:string ST_DISPENSACAO_ANTECIPADO:string CO_CLASSIF_SUBSTANCIA:string CO_GRUPO_FINANCIAMENTO:string TP_PRODUTO:string ST_RENAME:string DS_CATMAT:string NO_OPERADOR:string NU_PRODUTO_WS_HE:string CO_SEQ_PROCEDIMENTO_PRODUTO:string ;CO_PRODUTO;NU_PORTARIA;NO_ORGAO_EMISSOR;DT_PORTARIA;DT_INICIO_VIGENCIA;ST_VIGENCIA;VL_PROCEDIMENTO;QT_MAXIMA_UNIDADE;SG_UF;CO_ESFERA_ADMINISTRATIVA_SIGTAP;CO_MUNICIPIO_IBGE_SIGTAP;TP_SEXO;NU_IDADE_MAXIMA;NU_IDADE_MINIMA
```

Code to generate IBGE Social Classes CRUD Model:
```shell script
  $ rails g scaffold IBGE_SOCIAL_CLASSES cd_uf:integer nm_uf:string cd_municipio:integer nm_municipio:string numero_domicilios_classe_a:integer numero_domicilios_classe_b:integer numero_domicilios_classe_c:integer numero_domicilios_classe_d:integer numero_domicilios_classe_e:integer
```

Code to recreate populated database
```shell script
  $ rake db:recreate
```

Code to clone the extracted data
```shell script
  $ git clone data_sets data_sets
```

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

# Config Hosts

Open file:
```shell script
    nano /etc/hosts
```
And add those lines:
```
    127.0.0.1  docker discovery cassandra rabbit gateway documents oracle
    127.0.0.1  modulos redis identidades elk onlyoffice elasticsearch
    127.0.0.1  integracoes-gateway integracoes-documents integracoes-ui
    127.0.0.1  integracoes-modulos integracoes-identidades
    127.0.0.1  integracoes-configserver integracoes-discovery
```

# BREW - Latest ELK (Logstash Elastic Kibana)
## Installation
```shell script
   brew tap elastic/tap && brew install elastic/tap/kibana-full
```

## Starting Services
```shell script
   brew services start logstash && brew services start elasticsearch && brew services start kibana
```

# BREW - Stable ELK (Logstash Elastic Kibana)
## Installation
```shell script
   brew cask install homebrew/cask-versions/adoptopenjdk8 && brew install logstash && brew install elasticsearch && brew install kibana
```

## Starting Services
```shell script
   brew services start logstash && brew services start elasticsearch && brew services start kibana
```

## Checking Services
```shell script
    $ brew services list
```

## Running Services
#### Logstash
1. Setup driver (can change it at .conf in lib/dashboards)
    ```shell script
        $ cp drivers/mysql-connector-java-5.1.47-bin.jar /db_drivers
    ```    

1. Setup endpoint
    ```shell script
        $ cd lib
        $ /usr/local/Cellar/logstash/version/bin/logstash -f all_budget_info_logstash_mysql.conf 
    ```

1. Stop logstash (__if got error or died__): ^C (2x) --> Ctrl+C (2x)


1. Kill logstash processes
    ```shell script
        $ killall logstash
    ```

#### Elastic Search
1. Check logstash data in elastic search endpoint (default limit(size)=10)
    ```shell script
        $  curl 'http://localhost:9200/budget/_search?size=15&pretty=true'
    ```

#### Kibana
1. Check kibana dashboard
    ```shell script
        $  curl 'http://localhost:9200/projects/_search?size=1000&pretty=true'
    ```


3. Java MySQL Driver
    1. Download (JAR): http://www.java2s.com/Code/Jar/m/Downloadmysqlconnectorjava5123binjar.htm




RUN LOGSTASH
$ /usr/local/Cellar/logstash/6.4.2/bin/logstash -f 


CONFIG LOGSTASH
$ cd /usr/local/Cellar/logstash/6.4.2/libexec/config
$ sudo atom .
Alterações:
1. logstash.conf
    1. O que colocar neste arquivo em input para conectar ao mysql
        1. OFICIAL: https://www.elastic.co/guide/en/logstash/current/plugins-inputs-jdbc.html
        2. GRUPO: https://discuss.elastic.co/t/how-to-connect-mysql-database-by-logstash/61004/3
2. 
￼




CONFIG ELASTIC
$ 

CONFIG KIBANA
$ sudo nano /usr/local/etc/kibana/kibana.yml

server.port: 5601
elasticsearch.url: "http://localhost:9200”



Logstash conf sample

input {
  jdbc {
    jdbc_driver_library => "mysql-connector-java-5.1.36-bin.jar"
    jdbc_driver_class => "com.mysql.jdbc.Driver"
    jdbc_connection_string => "jdbc:mysql://localhost:3306/pbci_development"
    jdbc_user => "root"
    parameters => { "favorite_artist" => "Beethoven" }
    schedule => "* * * * *"
    statement => "SELECT * from pbci_development.projects" #where artist = :favorite_artist
    use_column_value => true
    tracking_column => "id"
  }
}

output {
  elasticsearch {
    hosts => ["http://localhost:9200"]
    index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
    #user => "elastic"
    #password => "changeme"
  }
  stdout { codec => rubydebug }
}
