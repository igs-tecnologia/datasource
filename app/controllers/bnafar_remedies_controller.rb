class BnafarRemediesController < ApplicationController
  before_action :set_bnafar_remedy, only: [:show, :update, :destroy]

  # GET /bnafar_remedies
  def index
    @bnafar_remedies = BnafarRemedy.all

    render json: @bnafar_remedies
  end

  # GET /bnafar_remedies/1
  def show
    render json: @bnafar_remedy
  end

  # POST /bnafar_remedies
  def create
    @bnafar_remedy = BnafarRemedy.new(bnafar_remedy_params)

    if @bnafar_remedy.save
      render json: @bnafar_remedy, status: :created, location: @bnafar_remedy
    else
      render json: @bnafar_remedy.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /bnafar_remedies/1
  def update
    if @bnafar_remedy.update(bnafar_remedy_params)
      render json: @bnafar_remedy
    else
      render json: @bnafar_remedy.errors, status: :unprocessable_entity
    end
  end

  # DELETE /bnafar_remedies/1
  def destroy
    @bnafar_remedy.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bnafar_remedy
      @bnafar_remedy = BnafarRemedy.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def bnafar_remedy_params
      params.require(:bnafar_remedy).permit(:DS_PRODUTO, :NU_CATMAT, :CO_GRUPO_FINANCIAMENTO)
    end
end
