class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def to_csv
    CSV.generate(headers: true) do |csv|
        csv << attributes.map{ |attr| user.send(attr) }
    end
  end

  # def to_csv_sample(from)
  #   cars = Car.where(maker_name: make)
  #
  #   CSV.generate(headers: true) do |csv|
  #     csv << attributes
  #
  #     cars.each do |car|
  #       csv << attributes.map{ |attr| car.send(attr) }
  #     end
  #   end
  # end
end
