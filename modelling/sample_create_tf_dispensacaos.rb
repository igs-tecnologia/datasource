class CreateTfDispensacaos < ActiveRecord::Migration[5.2]
  def change
    create_table :tf_dispensacaos do |t|
      t.integer :CO_CRM_PRESCRITOR, null: false
      t.integer :CO_CID, null: false
      t.integer :CO_TEMPO_VALIDADE, null: false
      t.integer :CO_TEMPO_DISPENSACAO, null: false
      t.integer :CO_MEDICAMENTO, null: false
      t.integer :CO_TIPO_PRODUTO, null: false
      t.integer :CO_PROGRAMA_SAUDE, null: false
      t.integer :CO_GEO_ESTABELECIMENTO, null: false
      t.integer :CO_GEO_SOLICITANTE, null: false
      t.integer :CO_ESTABELECIMENTO, null: false
      t.integer :CO_ESTAB_SOLICITANTE, null: false
      t.integer :CO_REGISTRO, null: false
      t.integer :NU_PROTOCOLO_ENTRADA, null: false
      t.integer :NU_CNS, null: false
      t.integer :NU_PESO, null: false
      t.integer :NU_ALTURA, null: false
      t.string :ST_VIVO, null: false, limit: 45
      t.integer :QT_DISPENSACAO, null: false
      t.string :NU_COMPETENCIA_DISPENSACAO, null: false
      t.integer :ST_ID_IDENTIFICACAO, null: false
      t.decimal :VL_UNITARIO, null: false, precision: 6, scale: 2
      t.decimal :VL_REFERENCIA_POPFARMA, null: false, precision: 6, scale: 2
      t.string :CO_ORIGEM_REGISTRO, null: false, limit: 1
      t.datetime :DT_CARGA, null: false
      t.integer :CO_TEMPO_NASCIMENTO, null: false
      t.integer :CO_TEMPO_OBITO, null: false
      t.integer :CO_PESSOA, null: false
      t.integer :SG_SEXO, null: false
      t.integer :CO_GEO_RESIDENCIA_PACIENTE, null: false
      t.integer :CO_SEQ_DISPENSACAO, null: false

      t.timestamps
    end
  end
end
