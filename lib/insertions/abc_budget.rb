require 'singleton'

module Insertions
  class ABCBudget < Facade
    include Singleton

    def initialize
      self.model = 'Budget'
      self.file_name = 'abc-budget.csv'
    end
  end
end