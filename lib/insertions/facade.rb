require 'singleton'

module Insertions
  class Facade
    include Singleton
    attr_accessor :file_name, :model, :specific_attrs, :additional_column, :additional_val

    def load_and_persist(show_progress=false, pagination=0)
      start_time = Time.now
      puts "Opening CSV at #{start_time}".colorize(:yellow)
      dataset_path = File.join(Rails.root, 'data_sets', self.file_name)

      count = 0
      headers = []
      registers = []
      excluded_attrs = %w(CO_MUNICIPIO_IBGE_PAC NU_REGISTRO_ANVISA QT_CNS_PACIENTE QT_CNES QT_CNPJ_FABRICANTE QT_PRESCRITOR QT_CID10_PRESCRICAO VL_MOVIMENTACAO)

      File.open(dataset_path) do |f| #, 'r:ISO-8859-1'
        while line = f.gets
          register = []
          line = line.strip
          line.gsub!('"', '') unless line.index('"').nil?
          columns = line.split(';')
          columns = line.split(',') if columns.length == 1
          skip_register = false

          # COORD comes from headers
          if count == 0
            headers = columns
            count = count+1
            next
          end

          columns.each_with_index do |val, i|
            attr = headers[i]
            next if excluded_attrs.include?(attr) || (!self.specific_attrs.nil? && !self.specific_attrs.include?(attr))
            if attr == 'NU_COMPETENCIA_DISPENSACAO'
              if val == ''
                skip_register = true
                break
              end

              self.additional_val.include?('w') ? val = "#{val[0..1]}/#{val[2..-1]}".to_date : val = "#{val[4..-1]}/#{val[0..3]}".to_date
            end

            if attr == 'MONTANTE_DISPENSADA'
              if val == ''
                val = 0
              end
            end

            next if (attr == 'NU_CATMAT' && val.index('BR').nil?) || val.nil? || (attr == 'DS_PRODUTO' && val.length < 10)

            if !attr.nil? && (!attr.index('vl').nil? || !attr.index('VL').nil?)
              if val.is_a?(String) && !val.index(',').nil?
                val.gsub! '.', ''
                val.gsub! ',', '.'
                val = '0' + val if val[0] == '.'
              end

              val = 0 if val == ''
              val_new = BigDecimal(val)
              val = val_new
            end

            register << val
          end

          if (!self.additional_column.nil? && self.additional_column.length > 0) && (!self.additional_val.nil? && self.additional_val.length > 0)
            self.additional_column.each_with_index do |add_col, i|
              register << self.additional_val[i]
            end
          end

          self.specific_attrs.nil? ? import_headers = headers - excluded_attrs : import_headers = self.specific_attrs
          import_headers += self.additional_column if !self.additional_column.nil? && self.additional_column.length > 0

          next if skip_register
          registers << register if register.length == import_headers.length
          count = count+1

          if show_progress
            if pagination == 0 || (count % pagination == 0)
              p "#{count} iterated registers at #{Time.now}"
              start_time = Time.now
              puts 'Insertion started'.colorize(:yellow)
              self.specific_attrs.nil? ? import_headers = headers - excluded_attrs : import_headers = self.specific_attrs
              import_headers += self.additional_column if !self.additional_column.nil? && self.additional_column.length > 0
              persist_result = self.model.classify.safe_constantize.import import_headers, registers, validate: false
              end_time = Time.now
              p "Spent Time (insertion): #{end_time - start_time}"

              # count == 1000000 ? break : count=count+1 # TODO Comment if not a test
              registers = [] #free memory
            end
            # break #comment to persist whole data
          end
        end

        unless show_progress
          self.specific_attrs.nil? ? import_headers = headers - excluded_attrs : import_headers = self.specific_attrs
          persist_result =  self.model.classify.safe_constantize.import import_headers, registers, validate: false
        end

        if show_progress
          puts "Insertion started (registers left/missing: #{registers.length})".colorize(:yellow)
          self.specific_attrs.nil? ? import_headers = headers - excluded_attrs : import_headers = self.specific_attrs
          import_headers += self.additional_column if !self.additional_column.nil? && self.additional_column.length > 0
          persist_result = self.model.classify.safe_constantize.import import_headers, registers, validate: false
          end_time = Time.now
          p "Spent Time (insertion): #{end_time - start_time}"
        end

        if persist_result
          persist_result.failed_instances.each do |failure|
            print failure.errors
          end
        end
      end
      puts "CSV #{file_name} finished at #{start_time}".colorize(:green)
    end
  end
end
