require 'singleton'

module Insertions
  class HBaseStratBNAFARDispensation < Facade
    include Singleton

    def initialize
      self.model = 'BNAFARDispensation'
      self.file_name = 'TF_DISPENSACAO.CGAFB-CGAFME.csv'
      self.additional_column = %w(ORIGIN)
      self.additional_val = %w(h)
    end
  end
end
